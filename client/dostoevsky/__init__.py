import sys
import json
import traceback
import requests
from datetime import datetime


class Dostoevsky(object):
    DEFAULT_PROJECT = "default"
    servers = None
    api_key = None
    project = None

    def __init__(self):
        self.servers = []

    def hook(self):
        """ Hooks to the global exception handler
        """
        sys.excepthook = self.handler

    def unhook(self):
        """ Sets the old value to the global exception handler
        """
        sys.excepthook = sys.__excepthook__

    def configure(self, servers=[], api_key=None, project=None):
        self.project = project if project else Dostoevsky.DEFAULT_PROJECT
        if not servers:
            raise ValueError("You need to specify at least one server")
        # check servers are alive
        for server in servers:
            if Dostoevsky.is_server_live(server):
                # TODO: think of a better way to build the final uri
                uri = "{}/api/projects/{}/issues".format(server, project)
                self.servers.append(uri)
        if not self.servers:
            raise ValueError("None of the servers provided is alive")
        # check the api key is valid
        if not api_key:
            raise ValueError("You need to provide API key")
        elif not Dostoevsky.is_api_valid(self.servers[0], api_key):
            raise ValueError("Invalid API key")
        self.api_key = api_key
        self.headers = {"Authorization": api_key}

    @staticmethod
    def get_exception_info():
        info = {}
        try:
            ex_type, ex_value, ex_tb = sys.exc_info()
            info = {
                "type": ex_type.__name__,
                "value": ex_value.message,
                "file": ex_tb.tb_frame.f_code.co_filename,
                "traceback": traceback.format_exc()}
        finally:
            ex_type, ex_value, ex_tb = None, None, None
        return info

    def handler(self, ex_type, ex_value, ex_tb):
        """ Called automatically whenever an unhandled exception occurs

        @param ex_type - Exception type
        @param ex_value - Exception value
        @param ex_tb - traceback
        """
        info = {
            "type": ex_type.__name__,
            "value": ex_value.message,
            "file": ex_tb.tb_frame.f_code.co_filename,
            "traceback": traceback.format_exc()}
        data = {
            "msg": info["value"],
            "date": unicode(datetime.now()),
            "extra": {},
            "exception": info}
        self._send(data)

    def send(self, msg, extra={}):
        """ Log data manually
        """
        info = Dostoevsky.get_exception_info()
        data = {
            "msg": msg,
            "date": unicode(datetime.now()),
            "extra": extra,
            "exception": info}
        self._send(data)

    def _send(self, data):
        # TODO: do this asynchronously
        # TODO: save data in case the request fails
        serialized = json.dumps(data)
        for server in self.servers:
            uri = "".format()
            try:
                response = requests.post(
                    server, data={"data": serialized}, headers=self.headers)
            except requests.exceptions.RequestException:
                # TODO: handle this error
                pass
            if not response:
                # try another server
                continue
            elif response.status_code != 200:
                continue
            break
        else:
            # TODO: log some other way
            pass

    @staticmethod
    def is_server_live(server):
        """ Ping the server to check it's alive
        @param string server - server URI
        @returns boolean status
        """
        # TODO: implement it
        return True

    @staticmethod
    def is_api_valid(server, key):
        """ Check the key against the server
        @param string server - server URI
        @param string key - API key to check
        @returns boolean status
        """
        # TODO: implement it
        return True
