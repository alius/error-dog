import sys
import pytest
from mock import patch

from dostoevsky import Dostoevsky


class TestClient(object):
    def test_configure_all_params(self):
        d = Dostoevsky()
        servers = ["http://192.168.1.1", "http://192.240.0.18"]
        api_key = "THIS_IS_API_KEY"
        project = "MyProject"
        api_urls = ["{}/api/projects/{}/issues".format(s, project) for
                    s in servers]
        d.configure(servers=servers, api_key=api_key, project=project)
        for s in d.servers:
            assert s in api_urls
        assert d.api_key == api_key
        assert d.project == project

    def test_configure_missing_servers(self):
        d = Dostoevsky()
        with pytest.raises(ValueError) as ex:
            d.configure()
        assert "You need to specify at least one server" in str(ex.value)

    @patch('dostoevsky.Dostoevsky.is_server_live')
    def test_is_server_live_called(self, patched_fn):
        d = Dostoevsky()
        with pytest.raises(ValueError):
            d.configure(servers=["127.0.0.1"])
        patched_fn.assert_called_once_with("127.0.0.1")

    @patch('dostoevsky.Dostoevsky.is_server_live')
    def test_configure_dead_servers(self, patched_fn):
        patched_fn.return_value = False
        d = Dostoevsky()
        with pytest.raises(ValueError) as ex:
            d.configure(servers=["127.0.0.1"])
        assert "None of the servers provided is alive" in str(ex.value)

    def test_configure_missing_api_key(self):
        d = Dostoevsky()
        with pytest.raises(ValueError) as ex:
            d.configure(servers=["127.0.0.1"])
        assert "You need to provide API key" in str(ex.value)

    @patch('dostoevsky.Dostoevsky.is_api_valid')
    def test_configure_invalid_api_key(self, patched_fn):
        patched_fn.return_value = False
        d = Dostoevsky()
        with pytest.raises(ValueError) as ex:
            d.configure(servers=["127.0.0.1"], api_key="123")
        assert "Invalid API key" in str(ex.value)

    @patch('dostoevsky.Dostoevsky._send')
    def test_send(self, mock_send):
        msg = "Division by zero"
        d = Dostoevsky()
        try:
            a = 1/0
        except:
            d.send(msg)
        args = mock_send.call_args[0][0]
        assert "msg" in args
        assert args["msg"] == msg
        assert "exception" in args
        assert args["exception"]["type"] == "ZeroDivisionError"
        assert args["exception"]["value"] == \
            "integer division or modulo by zero"
        assert "extra" in args
        assert len(args["extra"]) == 0

    def test_hook_unhook(self):
        current_handler = sys.__excepthook__
        d = Dostoevsky()
        d.hook()
        assert sys.excepthook == d.handler
        d.unhook()
        assert sys.excepthook == current_handler

    def test_is_server_live(self):
        # TODO: this is not implemented in code
        assert 0, "Not implemented"

    def test_is_api_key_valid(self):
        # TODO: this is not implemented in code
        assert 0, "Not implemented"
