# Dostoevsky

Dostoevsky (apart from being a great Russian writer) is also the name of the client library for Tolstoy (another great Russian writer).
```python
from dostoevsky import Dostoevsky

d = Dostoevsky()
d.configure(servers=("https://server1.com", "https://server2.com"), api_key="95251fdc9fdd3f92e7aed141fe7fd06b2aeb9dedb6f7ac08e7d9babc7e898b49", project="my_app")

def bad_code():
    return 1/0

try:
    bad_code()
except ZeroDivisionError:
    d.send("Something went wrong", extra={"fn": "bad_code"})
```

## Features

You can do the following:

* Log any message anywhere in the code
* Log message in the exception handler (exception information will be gathered automatically)
* Hook to the global exception handler so that missed exceptions will be handled as well
* Support for multiple servers for fault-toleranance

## Configuration

There are just three options for client configuration:

* `servers` - tuple with server addresses
* `api_key` - your API key
* `project` - your project ID, so that you could log from multiple apps using the same account and API key

## Installation

You need to first install the requirements from `requirements.txt` file, and then the library itself:
```bash
pip install -r requirements.txt
python setup.py install
```

## Logging messages

There are two ways messages are logged:

* automatically if global exception handler is used via `hook()`
* manually anywhere in the code via `send()` function

Send() receives two parameters: message and extras. Message is an arbitary string that you can send to describe the error. Extras is an optional parameter. It is dictionary that you can fill with any context information you need.

## Problems

When using the global exception handler via `hook()` function, the exception is silently handled and error report is sent to Tolstoy. However some cases might need that the exception be still raised with report sent.

## TODO

To see any tiny TODOs that are not mentioned in this readme file, run the following command (requires `ag` on your machine):
```bash
make todo
```

The list can be endless, but here are the most important things that are still missing from the client:

* Fall back to another logging mechanism
* Implement API key verification
* Implement server error handling in case an error is returned
* Retry sending if the server replies with an error
* Async send (currently code blocks until the request is finished)
* Test coverage

Functions in the code do not have proper documentation. A possible improvement would be to also add support for doctest.

## Testing

Py.test and tox are used for testing. Tests are located in the `tests` folder. There are several tests that fail because the functionality has not been implemented in the code yet.

To run tests do the following:
```bash
make test
```
or
```bash
tox
```
