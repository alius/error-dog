The repository contains code for error logging and aggregation server and library, both written in Python.

Currently only Python 2.7 has been tested and supported, however transition to Python 3 should be easy and quick.

For the documentation and installation instructions check readme files in the `client` or `server` folders.
