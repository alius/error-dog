import os
from setuptools import setup

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "Tolstoy",
    version = "0.0.1",
    author = "Artiom Vasiliev",
    author_email = "artjom@protonmail.com",
    description = ("Error and exception logging server"),
    license = "BSD",
    keywords = "error logging aggregation tolstoy",
    url = "https://bitbucket.org/alius/errordog",
    packages=["tolstoy", "tests"],
    #long_description=read("README.md"),
    classifiers=[
        "Development Status :: 1 - Planning",
        "Topic :: Utilities",
        "License :: OSI Approved :: BSD License",
    ],
    zip_safe=False,
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
)
