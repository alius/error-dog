import string
import random
from peewee import *


db = SqliteDatabase('tolstoy.db')


def _create_tables():
    db.connect()
    db.create_tables([User, Project, Issue, ApiKey])


def _kill_tables():
    db.connect()
    db.drop_tables([User, Project, Issue, ApiKey])


class BaseModel(Model):
    id = PrimaryKeyField()

    class Meta:
        database = db

    @classmethod
    def all(cls):
        return cls.select()


class User(BaseModel):
    # TODO: encrypt password on save
    # TODO: add password validation method
    name = CharField(max_length=100)
    email = CharField(max_length=64, unique=True)
    password = CharField(max_length=64)


class Project(BaseModel):
    project_id = CharField(max_length=12)
    user = ForeignKeyField(User)
    name = CharField(max_length=100)

    def generate_id(self):
        self.project_id = "".join(
            random.choice(string.ascii_uppercase + string.digits) \
                    for _ in range(12))


class Issue(BaseModel):
    project = ForeignKeyField(Project)
    date = DateTimeField()
    message = CharField(max_length=1024)
    extra = CharField(max_length=2048)  # TODO: create custom JSONField
    file = CharField(max_length=64)
    ex_type = CharField(max_length=24)
    ex_value = CharField(max_length=64)
    ex_traceback = CharField(max_length=2048)


class ApiKey(BaseModel):
    key = CharField(max_length=256)
    user = ForeignKeyField(User)
