from __future__ import absolute_import
import json
from datetime import datetime
from flask import Blueprint, request

import models

api_bp = Blueprint('API', __name__)


@api_bp.route("/projects/<project_id>/issues", methods=["POST"])
def add_issue(project_id):
    # TODO: verify API key
    # TODO: proper decorator for JSON response
    # TODO: validate form
    # TODO: validate json and handle errors
    # TODO: validate date
    try:
        project = models.Project.get(models.Project.project_id == project_id)
    except models.Project.DoesNotExist:
        return json.dumps({"status": "failed", "error": "Invalid project"})

    data = json.loads(request.form["data"])
    message = data["msg"]
    date = datetime.strptime(data["date"], "%Y-%m-%d %H:%M:%S.%f")
    extra = json.dumps(data["extra"])
    file = data["exception"]["file"]
    ex_type = data["exception"]["type"]
    ex_value = data["exception"]["value"]
    ex_traceback = data["exception"]["traceback"]

    issue = models.Issue(
        project=project, date=date, message=message, extra=extra,
        file=file, ex_type=ex_type, ex_value=ex_value,
        ex_traceback=ex_traceback)
    issue.save()
    return json.dumps({"status": "success"})
