from __future__ import absolute_import
from datetime import datetime, timedelta
from flask import (
    Blueprint, render_template, abort, request, redirect, make_response,
    url_for)
from jinja2 import TemplateNotFound
from peewee import fn

import models


web_bp = Blueprint('Web', __name__, template_folder="templates")


def _get_user():
    email = request.cookies.get("email")
    if not email:
        return None
    try:
        return models.User.get(models.User.email == email)
    except models.User.DoesNotExist:
        return None


@web_bp.route("/", methods=["GET"])
def index():
    # TODO: do a proper login page
    user = _get_user()
    if not user:
        return redirect("/register")
    else:
        return redirect("/projects")


@web_bp.route("/register", methods=["GET", "POST"])
def register():
    """ Form to register as a user
    """
    if request.method == "GET":
        return render_template("register.html")
    else:
        # TODO:
        # - check user doesn't already exist
        # - form validation
        # - CSRF
        name = request.form["name"]
        email = request.form["email"]
        password = request.form["password"]
        user = models.User(
            name=name, email=email, password=password)
        user.save()
        response = make_response(redirect("/projects"))
        # TODO: create proper auth system
        response.set_cookie("email", email)
        # TODO: use `url_for` for redirect
        return response


@web_bp.route("/projects/<project_id>/issues", methods=["GET"])
def get_issues(project_id):
    """ Get a paginated list of issues for the specific project
    """
    try:
        page = int(request.args.get("page", 1))
    except ValueError:
        page = 1
    issues = models.Issue.select().order_by(models.Issue.date.desc()).\
        paginate(page, 50)

    next_url = "{}?page={}".format(
        url_for("Web.get_issues", project_id=project_id),
        page+1)
    prev_url = "{}?page={}".format(
        url_for("Web.get_issues", project_id=project_id),
        max(1, page-1))

    return render_template("issues.html", issues=issues,
                           prev_url=prev_url, next_url=next_url)


@web_bp.route("/projects/<project_id>/issues/search", methods=["POST"])
def search_issues():
    """ Do a search across all of the issues for specific project
    """
    pass


@web_bp.route("/projects", methods=["GET", "POST"])
def get_projects():
    """ Get a list of all the projects
    """
    # TODO: add @require_login decorator
    user = _get_user()
    if not user:
        return redirect("/register")

    if request.method == "GET":
        projects = [p for p in models.Project.select().
                    where(models.Project.user == user)]
        return render_template("projects.html", projects=projects)
    else:
        """ TODO:
            - check project doesn't already exist
            - form validation
            - CSRF
        """
        name = request.form["name"]
        project = models.Project(name=name)
        project.generate_id()
        project.user = user
        project.save()
        # TODO: use `url_for` for redirect
        return redirect("/projects")


@web_bp.route("/projects/<project_id>", methods=["GET"])
def get_project(project_id):
    """ Get overview of the project and its stats (graphs, num errors, etc)
    """
    # TODO: add @require_login decorator
    user = _get_user()
    if not user:
        return redirect("/register")

    try:
        project = models.Project.get(models.Project.project_id == project_id)
    except models.Project.DoesNotExist:
        abort(404)

    # Fetch 10 latest issues
    issues = models.Issue.select().where(models.Issue.project == project).\
        order_by(models.Issue.date.desc()).limit(10)

    # Compile stats for the past 24h for the graph
    # TODO: this is an extremely slow and dumb way to do the aggregation,
    # but for the first version it will work
    stats = {"dates": [], "values": []}
    start = datetime.now() - timedelta(hours=25)
    for hour in xrange(25):
        range_start = start + timedelta(hours=hour)
        range_end = start + timedelta(hours=hour+1)
        count = models.Issue.select(fn.Count(models.Issue.id)).\
            where(models.Issue.date >= range_start).\
            where(models.Issue.date < range_end).scalar()
        stats["dates"].append(range_end.strftime("%Y-%m-%d %H:%M:%S"))
        stats["values"].append(str(count))
    plot = {"x": ",".join(["'{}'".format(d) for d in stats["dates"]]),
            "y": ",".join(stats["values"])}

    return render_template("overview.html", issues=issues, plot=plot,
                           project_id=project_id)
