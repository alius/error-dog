# Tolstoy

Tolstoy is the error logging server which receives data from clients and aggregates it. It is named after the famous Russian writer.

## Features

Below is the list of features that server currently supports:

* Adding new users via form registration
* Listing and adding user projects
* Viewing error stats per project (graph for the last 24h, last 10 errors)
* Listing all the messages saved per project, paginated

### API

There is only one API call at the moment which is adding new messages. A client sends POST requests to the following URL `/api/projects/<projectid>/issues` with the `data` parameter set to the message.

Data is a JSON-encoded object with the following structure:

* `msg` - user-created message
* `date` - date of the event
* `extra` - object, extra parameters set by user. For example application context or user id
* `exception` - object with the information about the exception

Exception parameter has the following structure:

* `file` - name of the file where error happened
* `type` - type of the error
* `value` - error value
* `traceback` - full traceback

To perform an API request you need to set the `Authorization` header to your API key. Currently there is no API key validation.

## Installation

You need to first install the requirements from `requirements.txt` file:
```bash
pip install -r requirements.txt
```
Then proceed to running the package:
```bash
make webserver
```
This will run the server in debug mode on your machine. It can be reached using http://127.0.0.1:5000/

The code has been written for Python 2.7. All of the libraries support Python 3, so the transition will be straightforward in case needed.

## Security

Currently almost any kind of security is lacking. There is a very relaxed cookie check which can be easily circumvented (also any user can be impersonated). In terms of security the following needs to be done:

* hashing user password in the database
* user login/logout
* user session
* secure cookies
* API authentication
* signing and verifying request signatures so that API data cannot be tampered during the travel
* form validation and CSRF protection

## TODO

To see any tiny TODOs that are not mentioned in this readme file, run the following command (requires `ag` on your machine):
```bash
make todo
```
In case the silver searcher is missing, you can do the following:
```bash
find tolstoy -type f -name "*.py" -print | xargs grep "TODO"
```

Here is the list of bigger features that were not implemented:

* search
* managing error bursts
* message aggregation
* message expiration

### Search

While implementing basic search would be trivial using `Peewee` and its `select().where()` methods, I decided not to. The reason for this is that I used SQLite database for prototyping. For the next version and production system I would use PostgreSQL with proper indexing and `JSON` type for storing `extra` parameter so that it can also be searched in.

For later Solr might be considered to help with the search.

### Error bursts and aggregation

This feature was left out as I didn't have time for implementation and setup. The way I would build is is to have the intermediary database server (e.g. Redis) which would keep all the data for a short period of time. A worker would check if there are a lot of requests with exact same error in a short period of time (say 1 minute), it would then use one message and store it in the database along with the count of messages in that period of time.

### Message expiration

This can be easily implemented either using the database tools (e.g. MongoDB can use expiration time for each document), or using a separate worker that would fetch the settings per project and remove expired records.

## Machine configuration

For production system Tolstoy should be run behind NGINX. For speeding things up [openresty](https://openresty.org/en/) can be used so that for simple tasks NGINX work on its own. For example checking Authorization header and validating it against the database.

## Tests

Py.test and tox are used for testing. Tests are located in the `tests` folder. Only API part of the server has been tested with 3 tests failing since these features have not been implemented yet.

To run tests do the following:
```bash
make test
```
or
```bash
tox
```
