import os
import sys
import json
import pytest
import tempfile
from mock import patch
from datetime import datetime

from peewee import *

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, "{}/{}/tolstoy".format(current_path, os.path.pardir))

from tolstoy import models
from tolstoy import app


class TestApi(object):
    def setup_method(self, method):
        # Create and set up the database
        _, self.db_path = tempfile.mkstemp()
        models.db = SqliteDatabase(self.db_path)
        try:
            models._kill_tables()
        except:
            pass
        models._create_tables()
        user = models.User(name="Artiom", email="email", password="1")
        user.save()
        project = models.Project(name="project", user=user)
        project.generate_id()
        project.save()
        self.project_id = project.project_id
        self.app = app.test_client()

    def teardown_method(self, method):
        os.unlink(self.db_path)

    def test_add_issue_invalid_method(self):
        rv = self.app.get("/api/projects/{}/issues".format(self.project_id))
        assert "405 Method Not Allowed" in rv.data

    def test_add_issue_invalid_project(self):
        rv = self.app.post("/api/projects/something/issues")
        assert "Invalid project" in rv.data

    def test_add_issue_invalid_project_json(self):
        rv = self.app.post("/api/projects/something/issues")
        data = json.loads(rv.data)
        assert "status" in data
        assert data["status"] == "failed"
        assert "error" in data
        assert data["error"] == "Invalid project"

    def test_add_issue_missing_params(self):
        # TODO: this is not implemented
        pass

    def test_add_issue_invalid_data_structure(self):
        # TODO: these checks are not implemented in code
        assert 0, "Not implemented"

    def test_add_issue_invalid_date(self):
        # TODO: this check is not implemented in code
        assert 0, "Not implemented"

    def test_invalid_api_key(self):
        # TODO: authentication is not implemented yet
        assert 0, "Not implemented"

    def test_add_issue(self):
        message = {
            "msg": "message here",
            "date": unicode(datetime.now()),
            "extra": {},
            "exception": {
                "file": "index.py",
                "type": "ValueError",
                "value": "Invalid input",
                "traceback": "Happened somewhere on line something"}
            }
        data = json.dumps(message)
        rv = self.app.post("/api/projects/{}/issues".format(self.project_id),
                           data={"data": data})
        assert "success" in rv.data
        assert "status" in rv.data

    def test_issue_added_to_db(self):
        message = {
            "msg": "message there",
            "date": unicode(datetime.now()),
            "extra": {"user": "Artiom", "session": "1a2b"},
            "exception": {
                "file": "index.py",
                "type": "ValueError",
                "value": "Invalid input",
                "traceback": "Happened somewhere on line something"}
            }
        data = json.dumps(message)
        rv = self.app.post("/api/projects/{}/issues".format(self.project_id),
                           data={"data": data})
        assert "success" in rv.data
        assert "status" in rv.data

        project = models.Project.get(
            models.Project.project_id == self.project_id)
        issue = models.Issue.select().\
            where(models.Issue.message == "message there").\
            where(models.Issue.project == project).get()

        assert issue.message == "message there"
        assert issue.project.project_id == self.project_id
